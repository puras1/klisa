#include "ros/ros.h"
#include "visualization_msgs/Marker.h"
#include "sensor_msgs/PointCloud2.h"
#include <stdio.h>
#include <sstream>
#include <pcl/point_cloud.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl/common/transforms.h>
#include <tf/transform_listener.h>
#include <pcl_ros/transforms.h>
#include <boost/thread/thread.hpp>
#include <pcl/point_types.h>
#include <pcl/filters/passthrough.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/conditional_removal.h>


typedef pcl::PointCloud<pcl::PointXYZ> PointCloud;
pcl::PointCloud<pcl::PointXYZ>::Ptr pcl_out_filtered (new pcl::PointCloud<pcl::PointXYZ>);
tf::TransformListener *tf_listener;
PointCloud pcl_out;
PointCloud::Ptr mycloudPtr; 
tf::StampedTransform transform;
PointCloud::Ptr cloud (new PointCloud);
double max = 0;


void transformListenerFunction(const PointCloud::Ptr msg) {	
	pcl_ros::transformPointCloud(*msg, pcl_out, transform);
}


void filteringXYZAxis() {
	mycloudPtr = pcl_out.makeShared();
    pcl::ConditionAnd<pcl::PointXYZ>::Ptr range_cond (new pcl::ConditionAnd<pcl::PointXYZ> ());

    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
      pcl::FieldComparison<pcl::PointXYZ> ("z", pcl::ComparisonOps::GT, 0.3)));
    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
      pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::LT, 12.0)));
    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
      pcl::FieldComparison<pcl::PointXYZ> ("x", pcl::ComparisonOps::GT, -12.0)));
    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
      pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::GT, -12.0)));
    range_cond->addComparison (pcl::FieldComparison<pcl::PointXYZ>::ConstPtr (new
      pcl::FieldComparison<pcl::PointXYZ> ("y", pcl::ComparisonOps::LT, 3.0)));
		
    pcl::ConditionalRemoval<pcl::PointXYZ> condrem;
    condrem.setCondition (range_cond);
    condrem.setInputCloud (mycloudPtr);
    condrem.filter (*pcl_out_filtered);
}

void countingZ() {
	 max = pcl_out_filtered->points[0].z;

    for (int i = 0; i < 150; i++) {
		max = pcl_out_filtered->points[i].z;
	}
}

void lidarDataCallback(const sensor_msgs::PointCloud2::ConstPtr& msg) {
	pcl::fromROSMsg(*msg, *cloud);
	transformListenerFunction(cloud);
	filteringXYZAxis();
	countingZ();
}


int main(int argc, char **argv) {

	int markerCounter = 0;
	ros::init(argc, argv, "god_pkg");
 	ros::NodeHandle node;
	ros::Publisher lidar_pub = node.advertise<visualization_msgs::Marker>("visualization_marker", 0);

	int num_circ = 10;
	ros::Rate loop_rate(10);
    visualization_msgs::Marker marker;
	ros::Subscriber lidar_sub = node.subscribe("points_raw", 1000, lidarDataCallback);
	tf_listener = new tf::TransformListener;
	tf_listener->waitForTransform("/base_link", "/velodyne", ros::Time::now(), ros::Duration(5.0));

	try {
		tf_listener->lookupTransform("/base_link", "/velodyne", ros::Time(0), transform);
    } catch (tf::TransformException ex) {
     	ROS_ERROR("%s",ex.what());
    	ros::Duration(1.0).sleep();
	}

	while (ros::ok()) {
		marker.header.frame_id = "base_link";
		marker.header.stamp = ros::Time::now();
		marker.ns = "my_namespace";
		marker.action = visualization_msgs::Marker::ADD;
		marker.type = visualization_msgs::Marker::CUBE;		

		if (pcl_out_filtered->points.size() > 300) {
	    	for(int i = 0; i < 300; i++) {
				marker.id = i;

				marker.pose.position.x = pcl_out_filtered->points[i].x ;
				marker.pose.position.y = pcl_out_filtered->points[i].y;
				marker.pose.position.z = max;

		  		marker.pose.orientation.x = 0.0;
		  		marker.pose.orientation.y = 0.0;
		  		marker.pose.orientation.z = 0.0;
		  		marker.pose.orientation.w = 1.0;

		  		marker.scale.x = 1.1;
		  		marker.scale.y = 1.1;
		 		marker.scale.z = 1.1;
		  		marker.color.a = 0.7;
		  		marker.color.r = 0.0;
		  		marker.color.g = 1.0;	
		  		marker.color.b = 0.0;
		
	   	       lidar_pub.publish(marker);
		  }
	 }

    	ros::spinOnce();
    	loop_rate.sleep();
   }

	return 0;
}

